# Verzweigung / Bedingungen

## Vergleichsoperatoren
| Oberator | Funktion |
| -------- | -------- |
| <        | kleiner  |
| >        | größer   |
| ==       | gleich   |
| !=       | ungleich |
|          |          |

## if - Verzweigung
Mit der if Verzweigung kann man eine Entscheidung treffen, wie das Programm fortgesetzt werden soll.
Bsp.:
```python 
a=input("gib eine Zahl ein")
a = int(a)
if a < 10:
	print("a ist kleiner als 10")
```
Wenn der Ausdruck der hinter dem if steht wahr ist dann werden die Befehle die hinter dem Doppelpunkt eingerückt stehen ausgeführt. Dh. in dem Beispiel wenn a kleiner als 10 ist.

## else
Will man auch etwas machen wenn die Bedingung bei if nicht zutrifft, dann benutzt man noch den else Befehl.
```python 
a=input("gib eine Zahl ein")
a = int(a)
if a < 10:
	print("a ist kleiner als 10")
else:
	print("a ist nicht kleiner als 10")
```


>[!question] Aufgabe 
Schreibe ein Programm in das du 2 Zahlen eingeben kannst und es dir zurückgibt ob die erste oder die zweite Zahl größer war oder ob sie gleich groß waren.

>[!question] Aufgabe 
Schreibe ein Programm welches überprüft, ob eine vom Benutzer eingegebene Zahl, durch 2 teilbar ist. 
Tipp: a % 3 gibt dir den Rest einer Division zurück. Dh. wenn a durch drei teilbar ist, dann ist der Rest = 0
