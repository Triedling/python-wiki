# Texte
## Textausgabe
Um Text auszugeben benutzt man den Befehl print().

```python
print("Hallo")
```

## Variablen
Variablen können in Python einfach folgendermaßen definiert werden.

```python
a=3
b=4.5
c="Hi"
```

Den Wert einer Variablen kann ich auch mit print ausgeben. 
Dann allerdings ohne Anführungszeichen.

```python
print(a)
print(b)
print(c)
```

Man kann auch mehrere Variablen mit einer print Anweisung ausgeben

```python
print(a,b,c)
```

oder auch mehrere Textstücke aneinander hängen

```python
a = "Apfel"
b = "brei"
print(a+b)
```

## Texteingabe

Um eine Eingabe in der Konsole abzufragen und  in eine Variable zu schreiben.

```python
a = input("gib was ein!")
print("Das wurde eingegeben: ",a)
```

>[!question] Aufgabe 
Schreibe ein Programm was nach deinem Namen fragt und dir dann antwortet: 
Name wie kann ich dir dienen?
Bei Name soll dann dein Name stehen.

## Texteingabe in eine Zahl umwandeln

>[!question] Aufgabe 
Kopiere folgendes Programm in Thonny und führe es aus. Was läuft schief?
```python 
a = input("gib eine Zahl zwischen 10 und 30 ein")
print(4 * a)
```


Wird mit input() etwas eingegeben, dann geht python davon aus, dass dies ein Text war. Möchte man mit der Eingabe rechnen, dann muss man diese zuerst in eine Zahl umwandeln.
Um Text in eine ganze Zahl umzuwandeln geschieht dies mit dem Befehl `int()`.
Um Text in eine Kommazahl umzuwandeln geschieht dies mit dem Befehl `float()`.

```python 
a = input ("gib eine ganze Zahl zwischen 1 und 20 ein")
a = int(a)
print("Das Doppelte von a ist: ", 2*a)
b = input (" gib eine Kommazahl zwischen 1 und 20 ein")
b = float(b)
print("Das Doppelte von a ist: ", 2*b)
```

>[!question] Aufgabe 
Schreibe ein Programm in das du 3 Zahlen eingeben kannst und es gibt dir die Summe der Zahlen zurück.


