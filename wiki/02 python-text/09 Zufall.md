# Zufall
## Zufallszahlen
Wenn man die Library random importiert, dann kann man sich Zufallszahlen erstellen.
Bsp.:
```python 
import random
a = random.randint(0,100)
print(a)
```
`random.randint(zahl1,zahl2)` erstellt Zufallszahlen von zahl1 bis  zahl2.

>[!question] Aufgabe 
Erstelle ein Ratespiel. Das Programm soll sich zu Beginn eine Zufallszahl ausdenken. Dann kann der Spieler eine Zahl raten. Wenn die Zahl des Spielers zu klein war, gibt das Programm aus:
Deine Zahl war zu klein.
Wenn die Zahl zu groß war:
Deine Zahl war zu groß
Wenn die Zahl richtig war:
Super du hast die richtige Zahl erraten.
Wenn die Zahl noch nicht richtig war, darf der Spieler weiter raten.

>[!question] Zusatzaufgabe: 
Sobald der Spieler die richtige Zahl erraten hat, wird ausgegeben, wie viele Versuche er benötigt hat.

## Zufallsliste
```python 
import random
dieListe = ["Apfle","Birne","Zwetschge","Erdbeere","Stachelbeere"]
a = random.choice(dieListe)
print(a)
```
>[!question] Aufgabe 
Erstelle ein Programm, welches aus der Schülerliste die man ins Programm schreibt, zwei Schüler zufällig ausgibt.

>[!question] Zusatzaufgabe 
Es soll nicht zweimal der gleiche Schüler ausgegeben werden können.