# Lösungen
## Text

```python 
a = input("Gib deinen Namen ein!")
print(a, " Wie kann ich dir dienen?")
```

```python 
a = input("gib eine Zahl ein")
b = input ("gib noch eine Zahl ein")
c = input ("gib noch eine dritte Zahl ein")
a = int(a)
b = int(b)
c = int(c)
print("Die Summe der Zahlen ist: ",a+b+c)

```
## Taschenrechner
```python 
a+b : addieren
a-b : subtrahieren
a*b : multiplizieren
a**2 : potenzieren
a/b : dividieren
round(b,0) : runden ohne Nachkommastelle
print(round(b,2))
print(a%2) 
```
## Verzweigungen

```python 
a = input("Gibe eine Zahl ein.")
a = int(a)
b = input("Gib eine zweite Zahl ein.")
b = int(b)
if a < b:
	print("Die erste Zahl war kleiner.)
if a > b:
	print("Die erste Zahl war größer.)
if a == b:
	print("Die Zahlen waren gleich.")

```

```python 
a = input("Gib eine Zahl ein und ich sage dir ob sie durch 2 teilbar ist.")
a = int(a)
if a%2==0:
	print("Die Zahl ist durch 2 teilbar.")
else:
	print("Die Zahl ist nicht durch 2 teilbar.")
	
```
## while
```python 
b = 0
summe = 0
while b != "fertig":
	b = input("gib eine Zahl ein, und wenn du fertig bist gib 'fertig' ein.")
	if b != "fertig":
		summe = summe + int(b)
print(summe)
```
## Zufallszahlen
### Zahlen Raten 1
```python 
from random import *
a = random.randint(0,100)
b = -2
while(a!=b):
	b = input("Gib eine Zahl ein.")
	if (a < b):
		print("Deine Zahl war zu groß.")
	if a>b :
		print("Deine Zahl war zu klein.")
print("Super du hast die Zahl erraten.")
```
### Zahlen Raten 2
```python 
from random import *
a = random.randint(0,100)
b = -2
counter = 0
while(a!=b):
	b = input("Gib eine Zahl ein.")
	counter = counter + 1
	if (a < b):
		print("Deine Zahl war zu groß.")
	if a>b :
		print("Deine Zahl war zu klein.")
print("Super du hast die Zahl erraten.")
```

