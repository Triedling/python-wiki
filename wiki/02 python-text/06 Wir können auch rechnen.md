# Python als Taschenrechner

```python
a = 9
b = 2.2
print(a+b)
print(a-b)
print(a*b)
print(a**2)
print(a/b)
print(round(b,0))
print(round(b,2))
print(a%2)
```
>[!question] Aufgabe 
Finde heraus was die Befehle im Programmbeispiel oben machen. 
Du kannst dazu die Zahlen variieren!




