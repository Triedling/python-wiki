# while Schleife

Eine while Schleife wird so lange ausgeführt so lange die Bedingung in der Klammer war ist.
```python 
a = 0
while (a<10):
	print(a)
	b = input(" gib eine Zahl ein")
	a = a + int(b)
print ("bin nicht mehr in der Schleife")
```
>[!question] Aufgabe 
Schreibe ein Programm, dass so lange deine Eingaben addiert, bis du fertig eingibst.

