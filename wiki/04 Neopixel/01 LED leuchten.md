# 01 Neopixel LEDs leuchten lassen

### Kurzes Beispielprogramm
```python 
import machine, neopixel
np = neopixel.NeoPixel(machine.Pin(28), 8)
np[0] = (255, 0, 0) 
np[1] = (0, 128, 0) 
np[2] = (0, 0, 64)
np.write()
```

## Bibliotheken importieren
```python 
import machine, neopixel
```
Ist dazu da, dass die Befehle mit denen wir die Neopixel LEDs steuern, nutzen können.

## Ein Liste von Neopixel Objekten erstellen

![](neopixelArray.drawio.svg)

## Einer LED im Neopixel Streifen eine Farbe zuordnen.

![](farben.drawio.svg)
### Beispiele
```python 
np[0] = (255, 0, 0) 
```
Die erste LED des Rings wird ganz hell rot programmiert.
```python 
np[1] = (0, 128, 0) 
```
Die zweite LED des Rings wird halb hell grün programmiert.
```python 

np[2] = (0, 0, 64)  
```
Die dritte LED des Rings wird viertel hell blau programmiert.

## Die LEDs anschalten
```python 
np.write() 
```
Die neue programmierten LEDs werden mit diesem Befehl angeschaltet.

## Aufgaben

>[!question] Frage
>Teste das Beispielprogramm

>[!question] Frage
>Finde heraus mit welchen Einstellungen die LEDs gelb, türkis oder pink leuchten.
