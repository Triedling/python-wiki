# 00 Anschließen der LED Strips an den Pico

- Verbinde GND von den LEDs mit GND vom Pico
- Verbinde +5V von den LEDs mit 3,3V vom Pico
- Verbinde Din von den LEDs mit GP28 vom Pico
   
![](image/neopixel-pico.png)