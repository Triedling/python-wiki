# 02 LEDs blinken

## Beispielprogramm
```python 
import machine, neopixel
from time import sleep
np = neopixel.NeoPixel(machine.Pin(4), 8)
while(True):
	np[0] = (255, 0, 0) 
	np.write()
	sleep(1)
	np[0] = (0,0,0)
	np.write()
	sleep(1)
```

## neue Befehle

```python 
from time import sleep 
```
Um Pausen machen zu können müssen wir Befehle für das Zeitmanagement importieren.

```python 
 sleep(1)
```

Dieser Befehl lässt den Microcontroller 1 Sekunde Pause machen

```python 
while(True): 
```
Alles was nach diesem Befehl *eingerückt* steht wird dauerhaft immer wieder wiederholt.

![](whileTrue.drawio.svg)
## Aufgaben:

>[!question] Aufgabe 
>Teste das Beispielprogramm.

>[!question] Aufgabe 
>Verändere das Programm so, dass nacheinander 4 Verschiedene Farben blinken.


 
