# 03 Neopixel Lauflicht
## Beispielprogramm

```python 
import machine, neopixel
from time import sleep
np = neopixel.NeoPixel(machine.Pin(4), 8)
while(True):
	for i in range(8):
		np[i] = (255, 0, 0) 
		np.write()
		sleep(0.5)
		np[i] = (0,0,0)
		np.write()
```

## Neue Befehle


```python 
for i in range(8):
```
Alles was nach diesem Befehl steht wird 8mal wiederholt. Dabei wird in die Zählvariable i jeweils die Nummer des Durchlaufs geschrieben. Achtung in der Informatik beginnen wir mit 0 zu zählen.
Dh. 0,1,2,3,4,5,6,7

```python 
np[i] = (255, 0, 0)
```
Statt einer Zahl steht jetzt in der eckigen Klammer die Zählvariable der for-Schleife. 
Dh. beim ersten mal steht dort eine 0, dann eine 1, und so weiter.
Dadurch wird immer eine andere LED an und aus geschaltet.

## Aufgaben
>[!question] Aufgabe 
>Teste das Beispielprogramm

>[!question] Aufgabe 
>Verändere das Beispielprogramm, so dass nach dem blinken einer LED eine Pause von 0,5 Sekunden gemacht wird, bevor die nächste LED leuchtet.

>[!question] Aufgabe 
>Verändere das Beispielprogramm so, dass nach einem Durchlauf mit roten LEDs ein Durchlauf mit grünen LEDs und dann noch einer mit blauen LEDs folgt. Danach soll es wieder von vorne beginnen mit den roten LEDs.

>[!question] Aufgabe 
>Verändere das Beispielprogramm so, dass nach einem Durchlauf das Lauflicht wieder zurückläuft.

