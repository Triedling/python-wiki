# Eigene Funktionen schreiben

## Funktionen
Wenn ich an verschiedenen Stellen immer wieder ein Quadrat zeichnen möchte, kann ich das Programmstück für Quadrate-Zeichnen in eine Funktion schreiben.
Eine Funktion ist ein Stück Programmcode, welcher einen Namen bekommt und über den Namen aufgerufen werden kann.

### Erstellen einer Funktion - Funktionsdefinition
- `def` ist der Befehl zum definieren einer Funktion
- hinter `def` steht der Name der Funktion
- alles was nach der `def` Zeile eingerückt steht gehört zu der Funktion
```python 

def zeichne_Quadrat():
	for a in range(4):
		forward(100)
		right(90)
```

### Aufrufen einer Funktion - Funktionsaufruf
Damit die Funktion ausgeführt wird, muss sie aufgerufen werden. Dies geschieht einfach indem man ihren Namen nennt.
```python 
zeichne_Quadrat()
```
>[!question] Aufgabe 
>Teste die Quadrat Funktion. Wenn alles klappt benutze sie um mehrere Quadrate in verschiedenen Farben zu zeichnen.


>[!question] Aufgabe 
>Erstelle eine Funktion, die ein Haus zeichnet. Das Haus besteht aus einem Quadrat und einem gleichseitigen Dreieck.

>[!question] Aufgabe 
>Zeichne 4 Quadrate die 50 Pixel groß sind und 100 Pixel Abstand zueinander haben, so dass immer 2 in einer Reihe stehen. 
Zwischen den Quadraten soll kein Strich zu sehen sein.


## Einer Funktion noch eine Nachricht mitgeben
Man kann bei der Funktionsdefinition so genannte Parameter definieren, die eine Nachricht für die Funktion enthalten. Diese Parameter werden in der Klammer erstellt und können dann in der Funktion benutzt werden.
Bsp.:
```python 
def farbiges_Quadrat(farbe):
	pencolor(farbe)
	forward(100)
	right(90)
farbiges_Quadrat('green')
farbiges_Quadrat('red')
```
>[!question] Aufgabe 
>Zeichne die Olympischen Ringe mit Hilfe einer Funktion die einen Kreis zeichnet und der du die farbe für den Ring mitgeben kannst.

>[!question] Aufgabe 
>Erstelle eine Funktion die unterschiedlich große Dreiecke zeichnen kann.
