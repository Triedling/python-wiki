# Erste Schritte mit der Turtle

## Das erste Programm

```python 
from turtle import *
forward(100)
```
>[!question] Aufgabe 
>Schreibe das Programm in den Thonny Editor und starte es mit dem grünen Pfeil. 
## Erklärungen
Eine Turtle ist eine Schildkröte die eine Spur hinterlässt wenn sie sich bewegt. 

## Turtle Importieren
Dieser Befehl muss immer am Anfang eines Turtle Programms stehen, damit man die Turtle Befehle benutzen kann.
```python
from turtle import *
```
## Kommentare
Kommentare sind dazu da Teile eines Programms zu erklären, damit man selbst oder jemand anders es besser versteht.

```python
# Kommentare werden in Python mit einem Hashtag markiert.
# in jeder Zeile des Kommentars muss am Anfang ein Hashtag sein.
# Kommentare haben keine Auswirkung auf das Programm.
```

## Turtle bewegen
```python

# 100 Pixel vorwärts bewegen
forward(100)

# 100 Pixe rückwärts bewegen
backward(100)

# 90 Grad nach rechts drehen
right(90)

# 90 Grad nach links drehen
left(90)
```

>[!question] Aufgabe 
>Steure die Turtle so dass ein Dreieck und ein Quadrat gezeichnet wird.