## Bewegen ohne Spuren zu hinterlassen
Die Turtle kann an eine andere Stelle gehen ohne Spuren zu hinterlassen. Dafür muss sie nur den Stift hoch nehmen. Wenn sie dann wieder zeichnen soll muss sie den Stift wieder herunter nehmen.

```python 
penup()
pendown()
```
>[!question] Aufgabe 
>Zeichne 3 Dreiecke mit einer Kantenlänge von 50 Pixeln die voneinander einen Abstand von 50 Pixeln haben.
