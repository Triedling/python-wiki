# Thonny Editor

## Starten von Programmcode
Gib folgenden Code in den Editor ein.
```python 
print("Hallo world") 
```

Und starte dein Programm mit drücken auf den grünen Pfeil.
![](Bildschirmfoto%20Starten.png)
Falls du dein Programm noch nicht gespeichert hattest, muss du jetzt einen Namen für dein Programm eingeben.


## Stoppen eines Programms
Durch drücken des roten Stopp Buttons stoppt man das aktuelle laufende Programm.
![](Bildschirmfoto%20Thonny%20Ende.png)

## Speichern eines Programms
![](Bildschirmfoto%20Thonny%20Speichern.png)
Unter "Datei - Speichern unter..." kann man ein Programm speichern.
## Öffnen eines Programms
![](Bildschirmfoto%20Thonny%20öffnen.png)