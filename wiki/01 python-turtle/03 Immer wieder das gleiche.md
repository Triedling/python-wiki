# for-Schleifen

## Arbeitserleichterung durch Wiederholungen


```python
from turtle import *
for a in range(5):
	forward (100)
	right(45)
```
Der Befehl `for a in range(5):` wiederholt alles 5 mal was danach eingerückt steht.

>[!question] Aufgabe 
>Wandle das Programm so ab, dass ein 6-eck mit Hilfe der for-Schleife gezeichnet wird.

>[!question] Aufgabe 
>Zeichne ein Dreieck mit Hilfe der for-Schleife.


>[!question] Aufgabe 
>Zeichne einen Kreis mit Hilfe der for-Schleife.
Tipp: einmal im Kreis herum sind 360°

>[!question] Aufgabe 
>Zeichne einen 5zackigen Stern mit Hilfe der for-Schleife

## Benutzung einer Zählvariablen
Um das folgende Bild zu zeichnen musst du die Zählvariable der for-Schleife benutzen.
```python 
from turtle import *
for a in range(30):
	forward (a*5)
	right(35)
```
Hier ist a die Zählvariable. 
Teste das Beispiel und überlege dir was die Zählvariable macht.

>[!question] Aufgabe 
>Programmiere das folgende Bild:
>![[Bildschirmfoto vom 2023-12-18 14-47-17.png]]




