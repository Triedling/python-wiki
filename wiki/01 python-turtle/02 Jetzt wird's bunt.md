# Farben und Strichbreite

## Strichbreite verändern

```python
# Strichbreite 5 Pixel
width(5)
```
## Strichfarbe verändern
```python 
# Strichfarbe rot setzen
pencolor('red') 
```
>[!question] Aufgabe
>Zeichne 3 Quadrate, ein blaues , ein grünes, ein braunes nebeneinander.


## Zeichnung mit Farbe füllen
Alles was zwischen dem Befehl `begin_fill()`und dem Befehl `end_fill()` steht wird mit der Farbe gefüllt die durch `fillcolor()` davor festgelegt wurde.
```python 
# legt die Füllfarbe fest
fillcolor('red')
# ab hier soll die Zeichnung gefüllt werden
begin_fill()
forward(100)
right(120)
forward(100)
right(120)
forward(100)
# bis hier soll die Zeichnung gefüllt werden
end_fill()

```
>[!question] Aufgabe: 
>Fülle die Quadrate aus der letzten Aufgabe mit der jeweiligen Farbe.

## Füllfarbe und Strichfarbe ändern
Der Befehl `color()` ändert die Strich und die Füllfarbe.
```python 
color("red") 
```
## Hintergrundfarbe verändern

```python
bgcolor('brown')
```


## RGB Farbcodes
Man kann als Farbe auch einen rgb Farbcode eingeben. Wobei die Zahlen von 0-255 bzw. von 0-1 gehen können, je nachdem wieviele Farbstufen man mit  `colormode()` eingestellt hat.
### 255 Farbstufen
```python 
colormode(255)
color(0,255,255)

```
### Farbstufen als Kommazahl von 0 - 1
```python 
colormode(1)
color(0,0.5,0.9)
```
## Farbnamen
Alternative kann man auch aus der folgenden Farbtabelle den Namen der Farbe raussuchen.

```python 
color('chartreuse') 
```
https://www.farb-tabelle.de/de/farbtabelle.htm

>[!question] Aufgabe
>Zeichne ein gelbes Haus mit rotem Dach, rosa Fenstern und einer rosa Tür auf einem hellblauen Hintergrund.

