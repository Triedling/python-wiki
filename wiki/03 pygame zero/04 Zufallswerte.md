# Zufallszahlen
```python 
import random

alice.x = random.randint(50,1150) 
```
Mit `import random` wird eine Library für Zufallszahlen geladen.
Der Befehl `random.randint(1, 100)` erzeugt eine ganzzahlige Zufallszahl zwischen 1 und 100.

>[!question] Aufgabe: 
>Lasse vom oberen Rand Dinge an zufälligen Stellen herunterfallen.

>[!question] Aufgabe: 
>Erstelle die erste Version eines Spiels, bei dem du eine Spielfigur am unteren Rand nach rechts und nach links steuern kannst. Von oben fallen an zufälligen Stellen Dinge herunter denen du ausweichen musst.

