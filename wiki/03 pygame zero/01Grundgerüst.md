# Das Grundgerüst
Die folgenden Befehle stehen in jedem pygamzero Spiel.
`import pgzrun`importiert die Befehle die man danach benutzten kann. 

Mit `WIDTH` und `HEIGHT` wird die Größe des Spielefensters festgelegt.


`pgzrun.go` startet das Spiel

```python
import pgzero
import pgzrun

WIDTH = 1200 
HEIGHT = 800

pgzrun.go()
```

>[!question] Aufgabe: 
>Erstelle ein Fenster, welches 1000 breit und 700 hoch ist.
