# Kollisionen
```python 
if ship.colliderect(bullet): 
	print("das Schiff wurde getroffen")
```
die obige Abfrage ist wahr falls der Actor ship mit dem Actor bullet kollidiert ist.

>[!question] Aufgabe:
>Erweitere dein Spiel durch eine Kollisionserkennung. Sobald ein herunterfallender Gegenstand deine Spielfigur trifft soll im Terminal mit `print()` Ende ausgegeben werden.


