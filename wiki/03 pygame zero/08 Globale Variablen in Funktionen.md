# Globale Variablen
Variablen die im gesamten Programm gültig sind nennt man globale Variablen.
Falls man globale Variablen innerhalb von Funktionen verändern möchte müssen diese innerhalb der Funktion als global definiert werden.

```python 
score = 0 
speed = 10 
ende = False 
def update(): 
	global score 
	global ende 
	global speed 
	score +=1 
	speed += 0.1 
```

>[!question] Aufgabe: 
>Füge in dein Spiel einen Score hinzu. Bei jedem Gegenstand dem du ausweichen konntest soll der Score um 1 erhöht werden.

>[!question] Aufgabe: 
>Füge in dein Spiel eine Variable ende hinzu. Sobald in der update Funktion erkannt wird dass deine Figur mit etwas kollidiert ist, sollen in der draw Funkion nicht mehr die Spielfiguren angezeigt werden.

