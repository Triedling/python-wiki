# Viele Actor in einer Liste erstellen

## Listen in Python
Man kann in Python Listen erstellen und darin verschieden Dinge ablegen, anhängen, entfernen...
>[!question] Aufgabe: 
>Teste folgendes Programm und versuche es zu verstehen. Danach verändere es so dass noch das Wort Ende an die Liste angehängt wird und die 1 entfernt wird.
```python 
a=[1,2,5,8]
for teil in a:
	print(teil)
a.append(7)
for teil in a:
	print(teil)
a.remove(2)
for teil in a:
	print(teil)
```

## Viele Actor Objekte erstellen
>[!question] Aufgabe: 
>Versuche folgendes Programm zu verstehen und auszuführen.

>[!warning] Achtung: 
>Du musst noch ein Bild mit dem Namen bat.png in den images Folder kopieren.

```python 
import pgzero
import pgzrun
import random

WIDTH = 1200
HEIGHT = 800

# erstellt eine leere Liste mit dem Namen bats
bats=[]

    
def draw():
# Zeichnet alle Fledermäuse in der Liste
    for b in bats:
        b.draw()

def create_bat():
# erstellt eine Fledermaus
    bat = Actor("bat")
# legt die x und y Koordinate einer Fledermaus zufällig fest
    bat.x = random.randint(0,1200)
    bat.y = random.randint(0,800)
# fügt diese eine Fledermaus zur Liste bats hinzu
    bats.append(bat)

# ruft create_bats 10 mal auf, dh. es gibt danach 10 Fledermäuse
for a in range(10):
    create_bat()

pgzrun.go()
```
>[!question] Aufgabe: 
>Verändere das Programm so dass noch 15 aliens auf dem Bildschirm erscheinen. Du kannst entscheiden wie deine aliens aussehen sollen. Aber sie müssen anderst aussehen, als die Fledermäuse.

>[!question] Aufgabe: 
>Verändere das Programm so, dass alle Feldermäuse sich nach rechts bewegen und wenn eine rechts ankommt, sie wieder nach ganz links "gebeamt" wird. 
> > [!info] Tipp: Du musst eine `update()` Funktion einfügen und in dieser mit einer for-Schleife alle Fledermäuse bewegen.

>[!question] Aufgabe: 
>Verändere das Programm so, dass die Fledermäuse die wieder nach links gebeamt werden links an einer zufälligen y-Position auftauchen.

## Actor Liste auf Kollision überprüfen
```python 
for b in bats:
	if b.coliderect(spieler):
		bats.remove(b)
```
Dieser Programmteil muss in der update Funktion stehen. Er überprüft alle Fledermäuse in der Liste ob sie mit dem Actor spieler kollidiert sind. Die, die kollidiert sind, werden entfernt.

>[!question] Aufgabe:
>Schreibe ein Spiel, bei dem sich 10 zufällig auf dem Bildschirm platzierte Fledermäuse hin und her Bewegen (sie bewegen sich abwechselnd nach rechts und nach links). Der Spieler muss diesen ausweichen. Bei Kollision ist das Spiel vorbei.