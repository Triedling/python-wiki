# Vorbereitungen
## Installieren von PygameZero in Thonny
Unter dem Menü Werkzeuge den Punkt verwalte Pakete auswählen.
![](Bildschirmfoto%20vom%202024-03-23%2010-36-18.png)
Im folgenden Fenster nach Pygame Zero bzw. nach pgzero suchen.
![](Bildschirmfoto%20vom%202024-03-23%2010-37-31.png)
pgzero anklicken und danach auf installieren klicken.
![](Bildschirmfoto%20vom%202024-03-23%2010-37-59.png)
## Installieren von PygameZero in der Konsole
In der Konsole kann man folgenden Befehl eingeben, damit PygameZero installiert wird. 
>[!warning] Dies muss man nur machen, wenn man pgzero `nicht` in Thonny installiert!
```bash 
pip install pgzero 
```

