# Tastatur Eingabe
```python


def update():
	if keyboard.left: 
		alice.x -= 5 
	if keyboard.right: 
		alice.x += 5
```
Die update() Funktion ist dafür da den Zustand des Spiels zu ändern.
In der update() Funktion kann man die Tastatureingaben abfragen.
Bsp.: `keyboard.left` gibt true zurück falls die Pfeiltaste nach links gedrückt wurde.

>[!question] Aufgabe: 
>Erstelle ein Actor Objekt, welches durch drücken der Pfeiltasten in alle Richtungen auf dem Bildschirm bewegt werden kann.

