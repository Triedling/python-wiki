# Timer
## Einmaliger Timer
Um nach einer gewissen Zeit im Spiel etwas geschehen zu lassen benutzen wir in pygame timer mit callback Funktionen.

An der Stelle wo der timer gestartet wird schreiben wir folgenden Befehl:

```python 
clock.schedule(mach_das,0.5) 
```

`mach_das` ist eine Funktion die nach 0,5 Sekunden aufgerufen wird.

### Das Bild eines Actors ändern


```python 
alien.image="neuesBild" 
```

## Beispiel:
Probiere folgendes Beispiel aus. Dazu muss du in den images Ordner zwei Bilder mit dem Namen alien1.png und alien2.png kopieren.

```python 
import pgzero
import pgzrun

WIDTH = 1200 
HEIGHT = 800

alien = Actor("alien1")
alien.pos = (200, 200)

def anders():
	alien.image = "alien2"
	clock.schedule(normal,1)

def normal():
	alien.image = "alien1"
	
def update():
	if keyboard.space:
		anders()
def draw():
	screen.clear()
	alien.draw()
	
pgzrun.go()
```
>[!question] Aufgabe: 
>Verändere das Programm so, dass das Alien 2 verschiedene  Bilder jeweils für 1 Sekunde zeigt, nachdem man die space Taste gedrückt hat.