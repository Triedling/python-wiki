# Text in einem Fenster anzeigen

Man kann in der draw() Funktion einen Text auf dem Bildschirm darstellen lassen.
Bspw. kann mit dem folgenden Befehl der aktuelle Score angezeigt werden.

```python 
def draw():
	screen.draw.text('score: '+str(score),(10,50),color=(0,255,0),fontsize=20) 
```

Der Befehl muss innerhalb der draw Funktion stehen. 
Er benötigt 4 Parameter die durch Kommas getrennt werden:
- einen Text in Form eines Strings, 
- der Wert von Variablen kann durch casting dargestellt werden: str(variable)
- die Position des Textes auf dem Bildschirm in Form eine Tuples
- die Farbe, diese kann als rgb Farbton angegeben werden
- die Schriftgröße in Pixel
>[!question] Aufgabe: 
>Zeige während des Spiels immer den aktuellen Score an. Wenn das Spiel Endet zeige auf dem Bildschirm Ende und den End-Score an.
