### viele Actor in einer Liste

```python 
import pgzero
import pgzrun
import random
WIDTH = 1200
HEIGHT = 800

bats=[]

    
def draw():
    screen.clear()
    for b in bats:
        b.draw()
def update():
    for b in bats:
        b.x = b.x +4
        if b.x > 1200:
            b.x = 0
            b.y = random.randint(0,800)
            
def create_bat():
    bat = Actor("bat")
    bat.x = random.randint(0,1200)
    bat.y = random.randint(0,800)
    bats.append(bat)

for a in range(10):
    create_bat()

pgzrun.go() 
```