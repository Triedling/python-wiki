# Die Spielobjekte
Die Spielobjekte werden als Actor bezeichnet.


## Erstellen eines Actor Objektes

```python
ship = Actor('alien') 
```
`ship` ist der Name des Objektes.
`alien` ist der Name der Bilddatei, wie das Objekt aussehen soll. 
Die Datei Endung der Bilddatei wird nicht angegeben.
Dh. wenn die Bilddatei `alien.png` heißt wird nur `alien` angegeben.

Die Bilddatei muss in einem Unterordner namens `images`, der im gleichen Ordner wie die Python Datei ist, abgelegt werden.
![](Bildschirmfoto%20vom%202024-03-23%2011-31-31.png)


## Den Actor positionieren

```python
ship.x = 150
ship.y = 750 
```
ship.x festlegen der X Koordinate des Actors
ship.y festlegen der Y Koordinate des Actors

>[!question] Aufgabe: 
>Finde heraus wo der Ursprung des Koordinatensystems liegt.
## Die Actor zeichnen
Die `draw()` Funktion wird regelmäßig automatisch aufgerufen um den Bildschirm neu zu zeichnen.
>[!warning] Es darf nur eine `draw`Funktion in einem Spiel geben!

Innerhalb dieser kann man dann die verschiedenen Actor mit `ship.draw()` oder `alien.draw()`, ... zeichnen.

```python
def draw():	
	ship.draw()
```

>[!question] Aufgabe: 
>Erstelle 2 Actor Objekte, die nebeneinander auf dem Bildschirm dargestellt werden.

## Den Zustand des Spiels verändern
Die `update()` Funktion wird regelmäßig automatisch aufgerufen, um den Zustand des Spiels zu ändern.
In dem folgenden Beispiel wird bei jedem Aufruf der `update()` Funktion der Wert von ship.x um 10 verändert.
>[! warning] Auch die update() Funktion darf nur einmal im Spiel stehen.

```python 
def update():
	ship.x = ship.x + 10
```
### Aufgabe:
Erstelle 1 Actor Objekt.
Es soll sich langsam von ganz links nach ganz rechts Bewegen.
Wenn es rechts den Bildschirm verlässt, soll es links wieder herein kommen.
>[!info] Eine if-Verzweigung hilft dir hier.
## Löschen des Fensters
Wenn man einen Actor bewegen möchte, zeichnet man ihn an einer neuen Stelle, damit nicht beide Bilder des Actors zu sehen sind, muss der Bildschirm am Anfang der draw Funktion gelöscht werden. 
>[!warning] Das löschen des Bildschirms sollte als aller erstes in der draw() Funktion stehen!

```python
def draw(): 
	screen.clear()
```

>[!question] Aufgabe: 
>Verbessere das letzte Programm, so dass keine Streifen zu sehen sind.

